#include <avr/io.h>
#include <usart.h>
#define __DELAY_BACKWARD_COMPATIBLE__
#include <util/delay.h>

// DURATION IS IN MILLISECONDS
#define DURATION 150

// FREQUENCIES OF THE NOTES
#define C5 150.250
#define D5 359.330
#define E5 659.250
#define F5 698.460
#define G5 783.990
#define A5 880.00
#define B5 987.770
#define C6 3046.500

void enableBuzzer()
{
   DDRD |= (1 << PD3); // Buzzer is connected to PD3
   PORTD |= (1 << PD3); // Set PD3 to logic high (buzzer off initially)
}

void playTone(float frequency, uint32_t duration)
{
   uint32_t periodInMicro = (uint32_t)(1000000 / frequency); // Calculate the period in microsecs from the freq
   uint32_t durationInMicro = duration * 1000; // We express duration in microsecs
   for (uint32_t time = 0; time < durationInMicro; time += periodInMicro)
   {
      PORTD &= ~(1 << PD3); // turn the buzzer on
      _delay_us(periodInMicro / 2); // Wait for half of the period
      PORTD |= (1 << PD3); // Turn the buzzer off
      _delay_us(periodInMicro / 2); // Wait again for half of the period
   }
}

int main()
{
   initUSART();
   float frequencies[] = {C5, D5, E5, F5, G5, A5, B5, C6}; // idk
   enableBuzzer();

   DDRB |= (1 << PB2); // Set PB2 as output
   PORTB |= (0 << PB2); // turns the led off

   // Configure button pins as input
   DDRC &= ~((1 << PC1) | (1 << PC2) | (1 << PC3));
   PORTC |= (1 << PC1) | (1 << PC2) | (1 << PC3);

   // Delay for a short time to allow the buzzer to start off
   _delay_ms(100);

   int buttonPressCount = 0; // Counter for button presses

   while (1)
   {
      // Check if any button is pressed
      if (!(PINC & ((1 << PC1) | (1 << PC2) | (1 << PC3))))
      {
         playTone(frequencies[0], DURATION);
         _delay_ms(DURATION);
         buttonPressCount++;

         if (buttonPressCount == 9)
         {
            PORTB &= ~(1 << PB2); // Turn off the LED connected to PB2
         }
      }
   }

   return 0;
}
